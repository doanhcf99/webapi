﻿using Dv_WebApi.Models.BusinessModel;
using Dv_WebApi.Models.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Dv_WebApi.Controllers
{
    [RoutePrefix("api/category")]
    public class CategoriesController : ApiController
    {
        CategoryModel cm;

        public CategoriesController()
        {
            cm = new CategoryModel();
        }
        [HttpGet]
        [Route("getall")]
        public IHttpActionResult GetAll(int? page = 1, string Name = null)
        {
            if (!String.IsNullOrEmpty(Name))
            {
                return Json(cm.GetByName(Name));
            }
            //int _page = page ?? 1;
            //int pageSize = 2;
            //var pageList = cm.GetAll()
            //    .OrderBy(x => x.Name)
            //    .Skip((_page - 1) * pageSize)
            //    .Take(pageSize).AsEnumerable();
            return Content(HttpStatusCode.OK, new ResponseData
            {
                Data = cm.GetAll(),
                Message = "Thành công",
                StatusCode = HttpStatusCode.OK
            });
        }
        [HttpPost]
        public IHttpActionResult Create(Category c)
        {
            try
            {
                cm.Add(c);
                return Json(new { messeage = "Ok" });
            }
            catch (Exception)
            {
                return Json(new { messeage = "Error" });
                
            }
        }
        [HttpPut]
        [Route("update")]
        public IHttpActionResult Update(Category c)
        {
            try
            {
                cm.Edit(c);
                return Json(new { messeage = "Ok" });
            }
            catch (Exception)
            {
                return Json(new { messeage = "Error" });


            }
        }
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                cm.Remove(id);
                return Json(new { messeage = "Ok" });
            }
            catch (Exception)
            {
                return Json(new { messeage = "Error" });


            }
        }

    }
}
