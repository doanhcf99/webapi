﻿using Dv_WebApi.Models.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dv_WebApi.Models.BusinessModel
{
    public class CategoryModel
    {
        ApiDbContext db;
        public CategoryModel()
        {
            db = new ApiDbContext();
        }
        public IEnumerable<Category> GetAll()
        {
            return db.Categories.AsEnumerable();
        }
        public Category Get(int id)
        {
            return db.Categories.Find(id);
        }
        public IEnumerable<Category> GetByName(string name)
        {
            return db.Categories.Where(x => x.Name.ToLower().Contains(name.ToLower())).AsEnumerable();
        }
        public bool Add(Category c)
        {
            try
            {
                db.Categories.Add(c);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            { 
                return false;
            }
        }
        public bool Edit(Category c)
        {
            try
            {
                db.Entry(c).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Remove(int id)
        {
            try
            {
                db.Categories.Remove(Get(id));
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}