﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Dv_WebApi.Models.DataModel
{
    public class ApiDbContext : DbContext
    {
        public ApiDbContext() : base("name=ApiContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApiDbContext, Migrations.Configuration>("ApiContext"));
        }
        public virtual DbSet<Category> Categories { get; set; }
    }
}