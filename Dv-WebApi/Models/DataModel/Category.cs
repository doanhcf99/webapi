﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Dv_WebApi.Models.DataModel
{
    public class Category
    {
        [Key]
        public int Id { get; set; }
        public string  Name { get; set; }
        public bool Status { get; set; }
    }
}