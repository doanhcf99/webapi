﻿using ProductService.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductService
{
    class Program
    {
        static void Main(string[] args)
        {
            StudentSerClient client = new StudentSerClient();
            int chose = 0;
            do
            {
                Console.WriteLine("Menu");
                Console.WriteLine("1.GetAll");
                Console.WriteLine("2.GetById");
                Console.WriteLine("3.Add");
                Console.WriteLine("4.Edit");
                Console.WriteLine("5.Delete");
                Console.Write("Nhap vao lua chon cua ban: ");
                chose = Convert.ToInt32(Console.ReadLine());
                switch (chose)
                {
                    case 1:
                        var data = client.GetAll();
                        foreach (var item in data)
                        {
                            Console.WriteLine("Thong tin sv:");
                            Console.WriteLine(item.Id);
                            Console.WriteLine(item.Name);
                            Console.WriteLine(item.Email);
                            Console.WriteLine("----------");
                        }
                        break;
                    case 2:
                        bool check = true;
                        while (check)
                        {
                            Console.Write("Nhap vao id: ");
                            var _st = client.GetById(Console.ReadLine());
                            check = (_st != null) ? false : true;
                            if (!check)
                            {
                                Console.WriteLine("Thong tin sv can tim:");
                                Console.WriteLine("Id: " + _st.Id);
                                Console.WriteLine("Name: " + _st.Name);
                                Console.WriteLine("Email:" + _st.Email);
                            }
                            else
                            {
                                Console.WriteLine("No data");
                            }
                        }
                        break;
                    case 3:
                        Student s = new Student();
                        Console.Write("Nhap id: ");
                        s.Id = Console.ReadLine();
                        Console.Write("Nhap name: ");
                        s.Name = Console.ReadLine();
                        Console.Write("Nhap email: ");
                        s.Email = Console.ReadLine();
                        client.Add(s);
                        break;
                    case 4:
                        Console.Write("nhap id can sua: ");
                        var st = client.GetById(Console.ReadLine());
                        Console.WriteLine("nhap name moi: ");
                        st.Name = Console.ReadLine();
                        Console.WriteLine("nhap email moi: ");
                        st.Email = Console.ReadLine();
                        client.Edit(st);
                        Console.WriteLine("sua thanh cong!!");
                        break;
                    case 5:
                        Console.Write("nhap id can xoa: ");
                        client.Remove(Console.ReadLine());
                        Console.WriteLine("Xoa thanh cong!!!");
                        break;
                        default:System.Environment.Exit(0);
                        break;
                }
            } while (chose > 0 && chose<6);
        }
    }
}
