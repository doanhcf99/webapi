﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WcfService1
{
    public class ApiDbContext : DbContext
    {
        public ApiDbContext() : base("name=CodeFirst")
        {

        }
        public DbSet<Student> Students { get; set; }
    }
}