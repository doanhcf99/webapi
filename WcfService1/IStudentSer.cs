﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IStudentSer" in both code and config file together.
    [ServiceContract]
    public interface IStudentSer
    {
        [OperationContract]
        void DoWork();
        [OperationContract]
        List<Student> GetAll();
        [OperationContract]
        Student GetByName(string name);
        [OperationContract]
        Student GetById(string id);
        [OperationContract]
        void Add(Student s);
        [OperationContract]
        void Edit(Student s);
        [OperationContract]
        void Remove(string id);

    }
    //[DataContract]
    //public class Student
    //{
    //    [DataMember]
    //    public string Id { get; set; }
    //    [DataMember]
    //    public string Name { get; set; }
    //    [DataMember]
    //    public string Email { get; set; }
    //}
}
