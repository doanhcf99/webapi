﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "StudentSer" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select StudentSer.svc or StudentSer.svc.cs at the Solution Explorer and start debugging.
    public class StudentSer : IStudentSer
    {
        private ApiDbContext db;
        public StudentSer()
        {
            db = new ApiDbContext();
        }
        public void DoWork()
        {
        }
        public void Add(Student s)
        {
            db.Students.Add(s);
            db.SaveChanges();
        }

        public void Edit(Student s)
        {
            var _s = GetById(s.Id);
            _s.Name = s.Name;
            _s.Email = s.Email;
            db.SaveChanges();
        }

        public List<Student> GetAll()
        {
            return db.Students.ToList();
           
        }

        public Student GetById(string id)
        {
            return db.Students.Find(id);
        }

        public Student GetByName(string name)
        {
            return GetAll().Where(x => x.Name.ToLower().Contains(name.ToLower())).FirstOrDefault();

        }

        public void Remove(string id)
        {
            var _st = GetById(id);
            db.Students.Remove(_st);
            db.SaveChanges();


        }
    }
}
